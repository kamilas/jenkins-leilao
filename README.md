# Jenkins

Integração Contínua - hábito de continuamente integrar o código.

Jenkins: Servidor para automatização de tarefas, como build, testes e deploy de aplicações.
 - Open Source
 - Extensível (Plugins)
 - Pode ser instalado no SO, em modo standalone ou através do Docker.

### Instalação
Download em [https://www.jenkins.io].
Para executar a versão standalone (.war) é necessári oter a versão 8 ou 11 do java instaladas.<br>
No diretório do arquivo executar o comando no prompt: `java -jar jenkins.war --httpPort=9090`.
A porta padrão é a 8080, e pode ser alterada adicionando `--httpPort=` ao comando.

Ao executar com sucesso será exibida uma senha que deverá ser inserida no primeiro acesso pelo navegador (http://localhost:8080)
```
Please use the following password to proceed to installation:
a6da26b4880a495ba33ebb194cc9....
```

 ![Primeiro acesso](/primeiro-acesso.png)
 
  - Para instalar plugins específicos: Select Plugins to Install > None (desmarcar todos).
 Sugestão marcar `Git`. 


 ### Job
 Job ou Item é a unidade de trabalho do Jenkins. Engloba as configurações.

 - Configurar ação ao realizar o build do projeto em Configurações, Build.
   ![Ação ao buildar](/build.png)

 - Automatizando Build
 Em Configurações, Build Triggers é possível selecionar o período. A opção SCM (Servidor de Código Fonte) verifica a cada perído determinado se houve alterações no branch da aplicação, em caso positivo executa o build.
 Em `Schedule` segue o formato de `Expressão Cron`, "*****" significa a cada 1 minuto.
 Para saber mais sobre [Cron](https://www.hostinger.com.br/tutoriais/cron-job-guia.).

   ![Trigger](/trigger.png)

 - Automatizando Pós Build
 Em `Post-build Actions` é possível selecionar ação a ser executada após o build, como execução dos testes, envio de notificações, realizar o build de outros jobs.
 A opção "Publish JUnit test result report" permitirá visualizar os relatórios de testes do JUnit do projeto.
 Em Test report XMLs são inseridos os arquivos que contém os relatórios do JUnit, é decorrente de cada projeto. Foi inserido "target/surefire-reports/*.xml".

 ### Plugins
 Plugins servem para extender as funcionalidades do Jenkins, adicionando novos recursos que não são nativos.
 Podem ser acessados clicando no logotipo Jenkins, Gerenciar Jenkins, Gerenciar Plugins. Cada plugin pode ser configurado de maneira distinta.

 "Radiator" é interessante para exibir em um monitor em tempo real para o time, fica verde em caso de sucesso e vermelho em caso de falha.<br>
 "Folder" é um tipo de Job que engloba outros jobs para organizá-los em diretórios. É possível adicionar um job criando no ícone de seta ao lado dele e na opção "Move".
 
### Deploy

##### Spring - Arquivo de propriedades específicas do ambiente
Podemos definir um arquivo `application-environment.properties` no diretório _src/main/resources_ e, em seguida, definir um perfil Spring com o mesmo nome de ambiente.<br>
Por exemplo, se definirmos um ambiente de "desenvolvimento", isso significa que teremos que definir um perfil dev e, em seguida, application-dev.properties.<br>
Este arquivo env será carregado e terá precedência sobre o arquivo de propriedades padrão (`application.properties`). 
Observe que o arquivo padrão ainda será carregado, mas quando houver um choque de propriedades, o arquivo de propriedades específico do ambiente terá precedência.

Exemplo de comando para execução do perfil prod: `java -Dspring.profiles.active=prod -jar target/leilao-0.0.1-SNAPSHOT.jar`

#### Build & Run

 - **Construir**: Adicionando mais um Job para ser responsável pelo build. Na opção Build vamos selecionar `Maven de alto nível`, goal `clean package` para criar o pacote target com executáveis.

 - **Executar**: Na opção Build iremos adicionar mais uma opção `Executar shell` para executar um script.
   `java -Dspring.profiles.active=prod -jar target/leilao-0.0.1-SNAPSHOT.jar`
   
 - No windows, é necessário inserir o comando do deploy como `Execute Windows Batch Command`, a opção Shell não é reconhecida.

Os comandos do build serão executados na ordem da lista.
Pode-se acessar a página http:localhost:8085/leiloes para verificar deploy da aplicação localmente.

 - O `&` é adicionado ao final para não travar o terminal. Se um comando `shell` ficar travado o Jenkins considera que o build nunca será finalizado.
   É possível saber mais detalhes desse problema no post [Process Tree Killer](https://wiki.jenkins.io/display/JENKINS/ProcessTreeKiller).
   
 - Ao final do build o Jenkins finaliza o processo, encerrando a aplicação.
   Para evitar isso adicionamos BUILD_ID="hack" (para opção `Shell Script`), que informa ao Jenkins q esse comando é associado a esse id (que pode ser qualquer palavra).
   Quando ele tentar finalizar o processo não será possível.

 ![Deploy Automatizado](/build-env.png)

### Controle de Acesso

Para criar um usário acessamos o Dashboard > Gerenciar Jenkins > Security > Gerenciar Usuários > Criar usuário.
Para configurar níveis de permissões, por padrão não há um controle mais específico de seleção de quais ações cada usuário pode realizar.
Para isso pode-se instalar o plugin, em Gerenciar Jenkins > Gerenciar Plugins, `Matrix Authorization Strategy`.

![Permissões](/permissoes.png)


### Notificações por e-mail
Em Dashboard > "Gerenciar Jenkins" > (System Configuration) Configurar o sistema, é possível acessar a opção `Notificação de E-mail` e inserir as configurações para recebimento de e-mail.

![Notificação por e-mail](/email-notification.png)

Acesse o Job/Item em questão, configurações, `Post-build Actions` ou `Ações de pós-build`.
Selecione `Adicionar ação de pós-build` > `Notificação de e-mail`. Em destinatários/recipients adicione os emails dos interessados que receberão notificações desse Job.
Caso tenha selecionado um sufixxo no primeiro passo (exemplo: "@gmail.com"), não é necessário colocar o email completo, apenas o prefixo.

### Notificações por Slack

Instale o plugin do Slack no Jenkins: "Gerenciar Jenkins" > "Gerenciar Plugins", selecione `Slack Notification`.
Para ativar retorne para "Gerenciar Jenkins" > (System Configuration) Configurar o sistema

![Slack plugin](/slack-jenkins-plugin.png)

No `Slack` é necessário instalar o app `Jenkins CI` para comunicação com o Jenkins. Clique no canto superior esquerdo, no nome do seu workspace, em Ferramentas > Gerenciar Apps.
Pesquise e instale o app "Jenkins CI", selecione o canal e clique em "Adicionar integração com Jenkins".<br>
Será exibido o ID da credencial do token de integração, que deve ser copiado e inserido no Jenkins, em Credential, como `secret text`.

![Aplicativo do Slack no Jenkins](/slack-app-jenkins.png)

Retorne ao Jenkins e faça a integração com Slack. Em Workspace insira o apenas o nome do workspace (sem slack.com).
![Integração Slack no Jenkins](/slack-on-jenkins.png)

Para habilitar em um Job acesse o Job/Item em questão, configurações, `Post-build Actions` ou `Ações de pós-build`.
Adicione uma ação do tipo `Slack notifications` e selecione em que situações deve ser enviada uma notificação.
 - Exemplo de mensagem do Jenkins no Slack:

![Exemplo de mensagem Jenkins](/mensagem-jenkins.png)

<hr>

#### Observação

 Após erro The system cannot find the file specified

 Jenkins, Gerenciar Jenkins, Shell, em Shell Executable inserir: `C:\Windows\system32\cmd.exe`